import asyncio
from nio import AsyncClient
import json

# Send a single feed item in one room in the RSS bot format
async def send_news(client, room, feedtitle, url, title):
    await client.room_send(
        room,
        message_type="m.room.message",
        content={
            "body": f"{feedtitle}: {title} ( {url} )",
            "format": "org.matrix.custom.html",
            "formatted_body": f'<strong>{feedtitle}</strong>:<br><a href="{url}"><strong>{title}</strong></a>',
            "msgtype": "m.notice",
            }
        )

async def main():

    #Load config
    with open("config.json") as config_file:
        config = json.load(open("config.json"))

    #Login
    client = AsyncClient(config["server"], config["mxid"])
    await client.login(config["password"])

    # Main
    with open("latest.json") as latest_file:
        latest = json.load(latest_file)
    while True:
        hit_latest = False

        # Download and read feed
        proc = await asyncio.create_subprocess_shell(f'/usr/bin/curl -s {config["feed"]}', stdout=asyncio.subprocess.PIPE)
        stdout, stderr = await proc.communicate()
        feed = json.loads(stdout.decode())
        
        for item in reversed(feed["items"]):

            if hit_latest:
                with open("latest.json", mode='w') as latest_file:
                    json.dump(item, latest_file)
                for room in config["rooms"]:
                    await send_news(client, room, feed["title"], item["url"], item["title"])
                    await asyncio.sleep(5) # Prevent rate limiting
                with open("latest.json") as latest_file:
                    latest = json.load(latest_file)
            
            elif latest["url"] == item["url"]:
                hit_latest = True
    
        await asyncio.sleep(300)

asyncio.get_event_loop().run_until_complete(main())

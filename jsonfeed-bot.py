import asyncio
import aiohttp
from nio import AsyncClient
import json
import datetime

def timestamp(item):
    date_str = item["date_published"]
    return datetime.datetime.fromisoformat(date_str).timestamp()

# Send a single feed item in one room in the RSS bot format
async def send_news(client, room, feedtitle, url, title):
    await client.room_send(
        room,
        message_type="m.room.message",
        content={
            "body": f"{feedtitle}: {title} ( {url} )",
            "format": "org.matrix.custom.html",
            "formatted_body": f'<h1>{feedtitle}:</h1><p><b><a href="{url}">{title}</a></b></p>',
            "msgtype": "m.notice",
            }
        )

async def main():

    # Load config
    with open("config.json") as config_file:
        config = json.load(config_file)

    # Login
    client = AsyncClient(config["server"], config["mxid"])
    client.access_token = config["access_token"]
    print("Logged in.")

    # Main
    while True:
        for f in config["feeds"]:
            hit_latest = False

            with open(f["file"]) as latest_file:
                latest = json.load(latest_file)

            # Download and parse feed
            async with aiohttp.ClientSession() as session:
                async with session.get(f["url"]) as response:
                    feed = json.loads(await response.text())

                    # Sort items by date
                    feed["items"].sort(key=timestamp)

            # Output latest feed items
            for item in feed["items"]:

                if hit_latest:
                    for room in f["rooms"]:
                        await send_news(client, room, feed["title"], item["url"], item["title"])
                        await asyncio.sleep(5) # Prevent rate limiting
                    with open(f["file"], mode='w') as latest_file:
                        json.dump(item["id"], latest_file)
                
                elif latest == item["id"]:
                    hit_latest = True

            if not hit_latest: # There is no latest item in the file yet, or the feed dropped the item
                for item in reversed(feed["items"]):
                    for room in f["rooms"]:
                        await send_news(client, room, feed["title"], item["url"], item["title"])
                        await asyncio.sleep(5) # Prevent rate limiting
                    with open(f["file"], mode='w') as latest_file:
                        json.dump(item["id"], latest_file)

        await asyncio.sleep(300)

asyncio.run(main())

# jsonfeed-bot

A Matrix bot for reading JSON feeds, using the same output format as the RSS integration.

Needs python3 and the matrix-nio library for Python. Two JSON files in the directory where it is run are required: `latest.json`, containing the latest item of the feed as a JSON object, and `config.json`, which needs a JSON array `"rooms"` of the rooms where the bot should post (the bot user needs to be in these rooms to be able to do so, it does not autojoin), as well as the variables `"mxid"`, `"password"` and `"server"`. 

Example for `config.json`:
```
{
    "mxid": "@mybot:myserver.tld",
    "server": "https://myserver.tld:8448",
    "password": "supersecretpasswordforthemxidofmybot",
    "feed": "https://myawesomeblog.xyz/feed.json",
    "rooms" [
        "!subscribedroomid1:myserver.tld",
        "!subscribedroomid2:matrix.org"
    ]
}
```
# jsonfeed-bot

A Matrix bot for reading JSON feeds and posting the latest items.

Needs python3 and the matrix-nio library for Python. In the directory where it is run, you must have `config.json` (see below on how to populate), as well as a file for each feed to store the latest item. You'll also need to make sure the bot is in all rooms you list, as it will not auto-join them.

Example for `config.json`:
```json
{
    "mxid": "@mybot:example.com",
    "server": "https://matrix.example.com",
    "access_token": "random_string_you_got_from_logging_in_at_server_probably_starts_with_syt_if_using_synapse",
    "feeds": [
        {
            "url": "https://example.com/feed.json",
            "file": "example.json",
            "rooms": [
                "!subscribedroomid1:example.com",
                "!subscribedroomid2:matrix.org"
            ]
        },
        {
            "url": "https://blog.example.com/feed.json",
            "file": "example2.json",
            "rooms": [
                 "!subscribedroomid3:example.com"
             ]
        }
    ]
}
```

- `"access_token"` can be obtained, for example, [via the Synapse admin API](https://matrix-org.github.io/synapse/latest/admin_api/user_admin_api.html#login-as-a-user), synadm (using [admin API](https://synadm.readthedocs.io/en/latest/synadm.cli.user.html#synadm-user-login) or [regular API](https://synadm.readthedocs.io/en/latest/synadm.cli.matrix.html#synadm-matrix-login)) or somehow manually via the standard Matrix client API, though we couldn't figure out how to do that last one. If you're not administrating a server, you can also get it from the Element devtools, or maybe other client's devtools/advanced options too.
- `server` should be the full client URI, not just the server name
- room aliases (starting with `#`) are not resolved, you need room IDs (starting with `!`), note these do change when a room is upgraded
- the `file` for each feed should contain the ID string of the latest post, if it doesn't exist when the bot is started, it will panic (for bootstrapping a feed, just put an empty string, i.e. `""` in it).
